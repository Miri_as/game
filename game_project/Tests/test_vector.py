from unittest import TestCase

from Logic.vector import Vector


class TestVector(TestCase):
    vector = Vector(199, 150)

    def test_x(self):
        assert self.vector.x == 199

    def test_y(self):
        assert self.vector.y == 150

    def test_copy(self):
        assert type(self.vector.copy()) == type(self.vector)

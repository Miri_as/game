from unittest import TestCase
from Logic.flappy_logic import FlappyLogic
from Logic.vector import Vector


class TestFlappyLogic(TestCase):
    logic = FlappyLogic()

    def test_inside(self):
        point = Vector(195, 199)
        assert self.logic.inside(point)
        point = Vector(200, 200)
        assert not self.logic.inside(point)

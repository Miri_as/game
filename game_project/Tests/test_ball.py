from unittest import TestCase

from Logic.ball import Ball
from Logic.vector import Vector


class TestBall(TestCase):
    vector = Vector(199, 150)
    ball = Ball(vector, 15, 20)

    def test_get_vector(self):
        assert self.ball.get_vector() == self.vector

    def test_get_size(self):
        assert self.ball.get_size() == 15

    def test_get_speed(self):
        assert self.ball.get_speed() == 20

"""Flappy, game inspired by Flappy Bird.
"""
from turtle import *

from IGame import IGame
from View.flappy_view import FlappyView
from Logic.flappy_logic import FlappyLogic


class Flappy(IGame):
    def __init__(self):
        self.flappy_view = FlappyView()
        self.flappy_logic = FlappyLogic()

    def setup_game(self):
        setup(420, 420, 370, 0)
        hideturtle()
        up()
        tracer(False)
        self.flappy_view.init_score(0)
        listen()
        onkey(lambda: self.flappy_logic.tap(0, 30), 'Up')
        onkey(lambda: self.flappy_logic.tap(0, -30), 'Down')
        onkey(lambda: self.flappy_logic.tap(30, 0), 'Right')
        onkey(lambda: self.flappy_logic.tap(-30, 0), 'Left')
        self.flappy_logic.move()
        done()

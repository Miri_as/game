from random import *
from Logic.ILogic import ILogic
from View.flappy_view import FlappyView

from Logic.ball import Ball
from Logic.vector import Vector

BIGGEST_SIZE = 50
SMALLEST_SIZE = 10
BIGGEST_SPEED = 10
SMALLEST_SPEED = 2


class FlappyLogic(ILogic):
    balls = []
    bird = Vector(0, 0)

    def __init__(self):
        self._score = 0
        self.view = FlappyView()

    def move(self):

        "Update object positions."

        self.bird.y -= 5
        for ball in self.balls:
            ball.get_vector().x -= ball.get_speed()

        if randrange(10) == 0:
            ball = self.initialization_ball()
            self.balls.append(ball)

        while len(self.balls) > 0 and not self.inside(self.balls[0].get_vector()):
            self._score += 5
            self.balls.pop(0)

        if not self.inside(self.bird):
            self.view.draw(False, self.bird, self.balls, self._score)
            return

        for ball in self.balls:
            if abs((ball.get_vector() - self.bird)) < 15:
                self.view.draw(False, self.bird, self.balls, self._score)
                return

        self.view.draw(True, self.bird, self.balls, self._score)
        self.move()

    def initialization_ball(self):
        y = randrange(-199, 199)
        vector_to_ball = Vector(199, y)
        size = randint(SMALLEST_SIZE, BIGGEST_SIZE)
        speed = randint(SMALLEST_SPEED, BIGGEST_SPEED)
        return Ball(vector_to_ball, size, speed)

    def tap(self, x, y):
        "Move bird up in response to screen tap."
        up = Vector(x, y)
        self.bird.move(up)

    def inside(self, point):
        "Return True if point on screen."
        return -200 < point.x < 200 and -200 < point.y < 200

    @property
    def score(self):
        return self._score

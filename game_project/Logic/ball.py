class Ball:
    def __init__(self, vector, size, speed):
        self.vector = vector
        self.size = size
        self.speed = speed

    def get_vector(self):
        return self.vector

    def get_size(self):
        return self.size

    def get_speed(self):
        return self.speed

    def set_vector(self, vector):
        self.vector = vector

    def set_size(self, size):
        self.size = size

    def set_speed(self, speed):
        self.speed = speed

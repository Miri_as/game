import flappy


def main():

    flappy_game = flappy.Flappy()
    flappy_game.setup_game()


if __name__ == "__main__":
    main()

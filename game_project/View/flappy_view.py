from turtle import *
from random import *
from View.IView import IView

BALL_COLOR = 'black'
BIRD_GOOD_COLOR = 'green'
BIRD_BAD_COLOR = 'red'
BIRD_SIZE = 10


class FlappyView(IView):
    _writer = Turtle(visible=False)

    def draw(self, alive, bird, balls, score):
        "Draw screen objects."
        clear()

        goto(bird.x, bird.y)

        if alive:
            dot(BIRD_SIZE, BIRD_GOOD_COLOR)
        else:
            dot(BIRD_SIZE, BIRD_BAD_COLOR)

        for ball in balls:
            goto(ball.get_vector().x, ball.get_vector().y)
            dot(ball.get_size(), BALL_COLOR)

        update()
        x = randint(15, 80)
        ontimer(self.write_score(score), x)

    def write_score(self, score):
        self._writer.undo()
        self._writer.write(score)

    def init_score(self, score):
        self._writer.color('white')
        self._writer.goto(160, 160)
        self._writer.color('black')
        self._writer.write(score)
